package ru.hldns.klaw.data.repository;

import org.springframework.data.repository.CrudRepository;
import ru.hldns.klaw.data.model.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmailAndOAuthUser(String email, boolean oauth);

    Optional<User> findByUsername(String name);

    boolean existsByUsername(String name);

    boolean existsByEmail(String name);
}
