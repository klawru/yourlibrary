package ru.hldns.klaw.data.utility;

public enum MetaDataField {
    ISSUED("ISSUED"),
    KEYWORD("KEYWORD"),
    GENRE("GENRE"),
    SERIES("SERIES"),
    SERIES_NUMBER("SERIES_NUMBER"),
    LIB_ID("LIB_ID"),
    LANG("LANG"),
    FILE_NAME("FILE_NAME"),
    TITLE("TITLE"),
    ISBN("ISBN");

    MetaDataField(String name) {
        this.name = name.toLowerCase();
    }

    private final String name;

    public String getName() {
        return name;
    }
}
