package ru.hldns.klaw.web.controller;

import lombok.Value;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.hldns.klaw.web.model.AuthorDTO;
import ru.hldns.klaw.web.results.Result;
import ru.hldns.klaw.web.results.ResultList;
import ru.hldns.klaw.web.service.AuthorsService;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = AuthorsPageController.class)
@ExtendWith(SpringExtension.class)
class AuthorsPageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthorsService service;


    @Test
    void getBooksPageReturn200() throws Exception {
        Mockito.when(service.getBooksByAuthor(eq("author"), anyInt(), anyInt()))
                .thenReturn(ResultList.of(
                        new AuthorDTOTest(145, "AuthorName"),
                                Page.empty(),
                                3)
                );
        mockMvc.perform(get("/author")
                .param("name", "author"))
                .andExpect(status().isOk());

        verify(service, times(1)).getBooksByAuthor(eq("author"), anyInt(), anyInt());
    }

    @Test
    void getBooksPageWhenEmptyReturn404() throws Exception {
        mockMvc.perform(get("/author"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void getSeriesPageReturn200() throws Exception {
        Mockito.when(service.getSeriesByAuthor(eq("author")))
                .thenReturn(Result.of(
                                new AuthorDTOTest(145, "AuthorName"),
                                Lists.emptyList()));
        mockMvc.perform(get("/author/series").param("name", "author"))
                .andExpect(status().isOk());

        verify(service, times(1)).getSeriesByAuthor(eq("author"));
    }


    @Value
    static final class AuthorDTOTest implements AuthorDTO {
        int id;
        String fullName;
    }
}