package ru.hldns.klaw.parsers.fb2.file;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

class FB2AnnotationParser {

    static String parseAnnotation(XMLStreamReader reader, String endElement) throws XMLStreamException {
        HTMLStreamWriter writer = new HTMLStreamWriter();
        try {
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLEvent.START_ELEMENT) {
                    translateXMLToHTML(reader, writer);
                } else if (event == XMLEvent.CHARACTERS) {
                    writer.writeCharacters(reader.getText());
                } else if (event == XMLEvent.END_ELEMENT) {
                    if (reader.getLocalName().equals(endElement))
                        break;
                    else if (!translateTag(reader.getLocalName()).isEmpty())
                        writer.writeEndElement();
                }
            }
            writer.replace("\n", "");
            return writer.getHTML().trim();
        } catch (IllegalStateException ignored) {
        }
        return null;
    }

    private static final String P = "p";
    private static final String EPIGRAPH = "epigraph";
    private static final String STRONG = "strong";
    private static final String EMPHASIS = "emphasis";
    private static final String A = "a";
    private static final String STRIKETHROUGH = "strikethrough";
    private static final String SUB = "sub";
    private static final String SUP = "sup";
    private static final String CODE = "code";
    private static final String IMAGE = "image";
    private static final String POEM = "poem";
    private static final String TITLE = "title";
    private static final String CITE = "cite";
    private static final String EMPTY_LINE = "empty-line";
    private static final String TEXT_AUTHOR = "text-author";
    private static final String SUBTITLE = "subtitle";
    private static final String STANZA = "stanza";
    private static final String V = "v";
    private static final String DATE = "date";

    private static final String TABLE = "table";
    private static final String TR = "tr";
    private static final String TH = "th";
    private static final String TD = "td";

    private static String translateTag(String tag) {
        switch (tag) {
            case P:
            case V:
                return P;
            case TITLE:
            case POEM:
            case STANZA:
            case TEXT_AUTHOR:
            case SUBTITLE:
            case DATE:
                return "div";
            case STRIKETHROUGH:
                return "s";
            case EMPTY_LINE:
                return "br";
            case A:
                return A;
            case CITE:
            case EPIGRAPH:
                return "blockquote";
            case STRONG:
                return "b";
            case EMPHASIS:
                return "em";
            case SUP:
                return SUP;
            case SUB:
                return SUB;
            case CODE:
                return CODE;
            case TABLE:
                return TABLE;
            case TR:
                return TR;
            case TH:
                return TH;
            case TD:
                return TD;
            default:
                return tag;
            case IMAGE:
                //ignored
                return "";
        }
    }

    private static void translateXMLToHTML(XMLStreamReader reader, HTMLStreamWriter writer) {
        final String tag = reader.getLocalName();
        switch (tag) {
            case A:
                writer.writeElement(translateTag(tag));
                writer.writeAttribute("href", "#");
                break;
            case EMPTY_LINE:
                writer.writeElement(translateTag(tag));
                writer.writeEndElement();
                writer.writeElement(translateTag(tag));
                break;
            default:
                writeTag(writer, translateTag(tag), "fb2-" + tag);
                break;
            case IMAGE:
                //ignored
                break;
        }
    }

    private static void writeTag(HTMLStreamWriter writer, String tag, String c) {
        writer.writeElement(tag);
        if (!c.isEmpty())
            writer.writeAttribute("class", c);
    }
}
