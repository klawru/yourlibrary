package ru.hldns.klaw.web.service;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.repository.BookRepository;
import ru.hldns.klaw.service.FileService;
import ru.hldns.klaw.utility.Pair;

import java.io.IOException;
import java.util.Optional;

@Service
public class BookPageService {
    private final BookRepository repository;
    private final FileService fileService;

    public BookPageService(BookRepository repository, FileService fileService) {
        this.repository = repository;
        this.fileService = fileService;
    }

    public Optional<Book> getBook(int id) {
        return repository.findById(id);
    }

    public Optional<Pair<StreamingResponseBody, String>> getStreamingResponseBook(int idBook, String idFile) throws IOException {
        Optional<Book> bookOptional = repository.findById(idBook);
        //Проверка того что файл верный
        if (bookOptional.isPresent() && bookOptional.get().getFile().equals(idFile)) {
            Book book = bookOptional.get();
            final var displayFileName = book.getDisplayFileName();
            return fileService.getFileStream(book.getFolder(), book.getFileName()).map(s -> new Pair<>(s, displayFileName));
        }
        return Optional.empty();
    }


}
