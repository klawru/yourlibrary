package ru.hldns.klaw.data.repository;

import org.springframework.data.repository.CrudRepository;
import ru.hldns.klaw.data.model.UserGroup;

import java.util.Optional;

public interface UserGroupRepository extends CrudRepository<UserGroup, Integer> {
    Optional<UserGroup> findByAuthority(String role);
}