package ru.hldns.klaw.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "lib")
public class ApplicationConfig {
    private String libraryDir;
    private String imgUploadDir;
    private String pathToInpxFile;
    private boolean disableSearch;
    private String thumbnailDir;
}
