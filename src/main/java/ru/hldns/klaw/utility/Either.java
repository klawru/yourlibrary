package ru.hldns.klaw.utility;


import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.function.Consumer;

/**
 * Объект-контейнер, который может содержать результат или ошибку
 *
 * @param <SUCCESS>
 * @param <ERROR>
 */
public abstract class Either<SUCCESS, ERROR> {

    public abstract boolean isPresent();

    public abstract SUCCESS getObject();

    public abstract ERROR getError();

    public abstract Either<SUCCESS, ERROR> ifPresent(Consumer<SUCCESS> consumerSuccess);

    public abstract Either<SUCCESS, ERROR> ifError(Consumer<ERROR> consumerFailure);

    @Value
    @EqualsAndHashCode(callSuper = false)
    public static class Success<SUCCESS, ERROR> extends Either<SUCCESS, ERROR> {

        private final SUCCESS object;

        @Override
        public boolean isPresent() {
            return true;
        }

        @Override
        public ERROR getError() {
            throw new IllegalStateException("getError() called on Success");
        }

        @Override
        public Either<SUCCESS, ERROR> ifPresent(Consumer<SUCCESS> consumerSuccess) {
            consumerSuccess.accept(object);
            return this;
        }

        @Override
        public Either<SUCCESS, ERROR> ifError(Consumer<ERROR> consumerFailure) {
            return this;
        }

    }

    @Value
    @EqualsAndHashCode(callSuper = false)
    public static class Error<SUCCESS, ERROR> extends Either<SUCCESS, ERROR> {

        private final ERROR error;

        @Override
        public boolean isPresent() {
            return false;
        }

        @Override
        public SUCCESS getObject() {
            throw new IllegalStateException("getObject() called on Failure");
        }

        @Override
        public Either<SUCCESS, ERROR> ifPresent(Consumer<SUCCESS> consumerSuccess) {
            return this;
        }

        @Override
        public Either<SUCCESS, ERROR> ifError(Consumer<ERROR> consumerFailure) {
            consumerFailure.accept(error);
            return this;
        }

    }

    public static <SUCCESS, ERROR> Either<SUCCESS, ERROR> error(ERROR failure) {
        return new Error<SUCCESS, ERROR>(failure);
    }

    public static <SUCCESS, ERROR> Either<SUCCESS, ERROR> success(SUCCESS success) {
        return new Success<>(success);
    }

}