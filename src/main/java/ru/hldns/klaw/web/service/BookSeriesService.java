package ru.hldns.klaw.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.Series;
import ru.hldns.klaw.data.repository.BookRepository;
import ru.hldns.klaw.data.repository.BookSeriesRepository;
import ru.hldns.klaw.web.model.BookDTOMin;
import ru.hldns.klaw.web.results.PaginatedResult;
import ru.hldns.klaw.web.results.Result;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static ru.hldns.klaw.web.WebUtility.getPageNum;

@Service
public class BookSeriesService {
    private final BookSeriesRepository seriesRepository;
    private final BookRepository bookRepository;
    private final Sort sortByName = Sort.by(Sort.Direction.ASC, "name");

    @Autowired
    public BookSeriesService(BookSeriesRepository repository, BookRepository bookRepository) {
        this.seriesRepository = repository;
        this.bookRepository = bookRepository;
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Result<BookDTOMin.SeriesDTO, BookDTOMin[]> findBookBySeries(String name) {
        final List<BookDTOMin> books = bookRepository.findBySeriesNameOrderByTitle(name, BookDTOMin.class);
        if (books.isEmpty())
            return Result.empty();
        BookDTOMin[] array = books.toArray(BookDTOMin[]::new);
        Arrays.sort(array, Comparator.comparingInt(BookDTOMin::getSeriesNumber));
        return Result.of(array[0].getSeries(), array);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PaginatedResult<Page<Series>> findBookSeriesByLetter(String request, int pageNum, int size) {
        pageNum = getPageNum(pageNum);
        final char letter = Character.toLowerCase(request.charAt(0));
        Page<Series> series;
        if (Character.isAlphabetic(letter) || Character.isDigit(letter))
            series = seriesRepository.findByNameLike(Character.toString(letter), PageRequest.of(pageNum, size, sortByName));
        else
            series = seriesRepository.findByFullNameLikeOther(PageRequest.of(pageNum, size));
        return PaginatedResult.of(series, 3);
    }
}
