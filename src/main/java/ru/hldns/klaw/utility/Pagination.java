package ru.hldns.klaw.utility;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;

/**
 * Класс утилита нужный для отбражения количества страниц с результатами
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Pagination {
    public int[] next;
    public int[] previous;
    public int now, lastPage;

    private Pagination(int current, int last, int maxLength) {
        this.now = current;
        this.lastPage = last;
        previous = getArray(current, 1, maxLength);
        next = getArray(current, lastPage, maxLength);
    }

    /**
     * @param current   текущая страница (отсчет с 1)
     * @param last      последняя страница
     * @param maxLength Количество страниц нужное для отображения слева и справа от текущей страницы
     */
    public static Pagination of(int current, int last, int maxLength) {
        return new Pagination(current, last, maxLength);
    }

    public static Pagination of(int current, int total, int pageSize, int maxLength) {
        return new Pagination(
                current,
                total == 0 ? 1 : (int) Math.ceil((double) total / (double) pageSize),
                maxLength);
    }

    public static <T> Pagination of(@NonNull Page<T> page, int maxLength) {
        return new Pagination(page.getNumber() + 1,
                Math.max(page.getTotalPages(), 1),
                maxLength);
    }


    private int[] getArray(int from, int to, int maxLength) {
        if (from == to)
            return EMPTY;
        var size = Math.min(Math.abs(to - from), maxLength);
        from = from < to ? from : from - size - 1;
        int[] number = new int[size];
        for (int i = 0; i < size; i++) {
            number[i] = ++from;
        }
        return number;
    }

    public boolean hasNext() {
        return next != EMPTY;
    }

    public boolean hasPrevious() {
        return previous != EMPTY;
    }

    public boolean isFirst() {
        return now == 1;
    }

    public boolean isLast() {
        return now == lastPage;
    }

    private static final int[] EMPTY = {};
}