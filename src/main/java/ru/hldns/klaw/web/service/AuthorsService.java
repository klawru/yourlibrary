package ru.hldns.klaw.web.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.repository.AuthorsRepository;
import ru.hldns.klaw.data.repository.BookRepository;
import ru.hldns.klaw.data.repository.SeriesAuthorRepository;
import ru.hldns.klaw.utility.CacheEvictEvent;
import ru.hldns.klaw.web.model.AuthorDTO;
import ru.hldns.klaw.web.model.AuthorNameDTO;
import ru.hldns.klaw.web.model.BookDTOMin;
import ru.hldns.klaw.web.model.SeriesDTO;
import ru.hldns.klaw.web.results.Result;
import ru.hldns.klaw.web.results.ResultList;

import java.util.List;
import java.util.Optional;

import static ru.hldns.klaw.web.WebUtility.getPageNum;

@Service
@Slf4j
public class AuthorsService {

    private final AuthorsRepository authorsRepository;
    private final BookRepository bookRepository;
    private final SeriesAuthorRepository seriesRepository;

    @Autowired
    public AuthorsService(AuthorsRepository authorsRepository,
                          BookRepository bookRepository,
                          SeriesAuthorRepository seriesRepository) {
        this.authorsRepository = authorsRepository;
        this.bookRepository = bookRepository;
        this.seriesRepository = seriesRepository;
    }

    private static final Sort sortByTitle = Sort.by(Sort.Direction.ASC, "title");

    @Cacheable(cacheNames = "AuthorBooks")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ResultList<AuthorDTO, Page<BookDTOMin>> getBooksByAuthor(String fullName, int page, int size) {
        var optional = getAuthorDTO(fullName);
        if (optional.isEmpty())
            return ResultList.empty();
        final AuthorDTO author = optional.get();
        page = getPageNum(page);
        final Page<BookDTOMin> books = bookRepository.findAllByAuthorsId(author.getId(), PageRequest.of(page, size, sortByTitle));
        return ResultList.of(author, books, 3);
    }

    private static final Sort sortByName = Sort.by(Sort.Direction.ASC, "name");

    @Cacheable(cacheNames = "AuthorSeries")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Result<AuthorDTO, List<SeriesDTO>> getSeriesByAuthor(String fullName) {
        final Optional<AuthorDTO> optionalAuthor = getAuthorDTO(fullName);
        if (optionalAuthor.isEmpty())
            return Result.empty();
        final AuthorDTO author = optionalAuthor.get();
        final var seriesDTOS = seriesRepository.getAllByIdAuthor(author.getId(), sortByName);
        return Result.of(optionalAuthor.get(), seriesDTOS);
    }

    private Optional<AuthorDTO> getAuthorDTO(String fullName) {
        return authorsRepository.findByFullName(fullName, AuthorDTO.class);
    }

    public ResultList<String, Page<AuthorNameDTO>> findByFirstLetter(String request, int pageNum, int size) {
        pageNum = getPageNum(pageNum);
        final char letter = Character.toLowerCase(request.charAt(0));
        if (Character.isAlphabetic(letter) || Character.isDigit(letter)) {
            final var s = Character.toString(letter);
            final var authorPage = authorsRepository.findByFullNameLike(s, PageRequest.of(pageNum, size));
            return ResultList.of(s.toUpperCase(), authorPage, size);
        } else {
            final var authorPage = authorsRepository.findByFullNameOther(PageRequest.of(pageNum, size));
            return ResultList.of("*", authorPage, size);
        }
    }

    @EventListener
    public void handleEvent(CacheEvictEvent cacheEvictEvent) {
        try {
            seriesRepository.refreshView();
        } catch (JpaSystemException e) {
            //Always return org.springframework.orm.jpa.JpaSystemException: could not extract ResultSet
            log.trace("Series refresh View :", e);
        }
    }
}