package ru.hldns.klaw.data.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "epersongroup")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"authority"})
public class UserGroup implements GrantedAuthority, Serializable {
    @Id
    @GeneratedValue
    @Column(name = "eperson_group_id",updatable = false,nullable = false)
    @With
    private UUID id;
    @Column(name = "name")
    private String authority;
}
