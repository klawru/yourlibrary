package ru.hldns.klaw.parsers.fb2.file;


import lombok.ToString;
import ru.hldns.klaw.utility.Stack;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static ru.hldns.klaw.parsers.fb2.file.HTMLStreamWriter.Cursor.*;

@ToString(of = {"builder", "cursor"})
public class HTMLStreamWriter {
    private final StringBuilder builder = new StringBuilder();
    private final Stack<String> stack = new Stack<>();
    private Cursor cursor = CHARACTERS;
    private final Set<String> emptyElements = new HashSet<>(Arrays.asList(
            "area", "base", "br", "col", "embed", "hr", "img", "input",
            "keygen", "link", "meta", "param", "source", "track", "wbr"));

    public void writeElement(String tag) {
        stack.push(tag);
        processing(START_ELEMENT);
        builder.append(tag);
    }

    public void writeEndElement() {
        final String tag = stack.pop();
        if (emptyElements.contains(tag))
            processing(END_SINGLE_ELEMENT);
        else {
            processing(END_ELEMENT);
            builder.append(tag);
        }
    }

    public void writeAttribute(String attribute, String value) {
        processing(ATTRIBUTE);
        builder.append(attribute).append("=\"").append(value).append("\"");
    }

    public void writeCharacters(String text) {
        processing(CHARACTERS);
        builder.append(text);
    }


    public String getHTML() {
        for (int i = 0; i < stack.size(); i++) {
            writeEndElement();
        }
        return builder.toString();
    }

    public void replace(String match, String replacement) {
        int index = -1;
        while ((index = builder.indexOf(match, index)) != -1) {
            builder.replace(index, index + match.length(), replacement);
        }
    }

    private void processing(Cursor nextElement) {
        switch (cursor) {
            case ATTRIBUTE:
            case START_ELEMENT:
                switch (nextElement) {
                    case START_ELEMENT:
                        builder.append("><");
                        break;
                    case END_SINGLE_ELEMENT:
                    case CHARACTERS:
                        builder.append(">");
                        break;
                    case END_ELEMENT:
                        builder.append("></");
                        break;
                    case ATTRIBUTE:
                        builder.append(" ");
                        break;
                }
                break;
            case END_SINGLE_ELEMENT:
            case CHARACTERS:
                switch (nextElement) {
                    case START_ELEMENT:
                        builder.append("<");
                        break;
                    case END_ELEMENT:
                        builder.append("</");
                        break;
                    case CHARACTERS:
                        break;
                    case END_SINGLE_ELEMENT:
                    case ATTRIBUTE:
                        throw new IllegalStateException("Unexpected  cursor: " + nextElement + " after cursor='" + cursor + "'");
                }
                break;
            case END_ELEMENT:
                switch (nextElement) {
                    case START_ELEMENT:
                        builder.append("><");
                        break;
                    case END_ELEMENT:
                        builder.append("></");
                        break;
                    case CHARACTERS:
                        builder.append(">");
                        break;
                    case END_SINGLE_ELEMENT:
                    case ATTRIBUTE:
                        throw new IllegalStateException("Unexpected  cursor: " + nextElement + " after cursor='" + cursor + "'");
                }
                break;
        }
        cursor = nextElement;
    }

    enum Cursor {
        START_ELEMENT,
        END_SINGLE_ELEMENT,
        END_ELEMENT,
        CHARACTERS,
        ATTRIBUTE
    }
}

