package ru.hldns.klaw.data.model;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class AuthorTest {

    @Test
    public void ofLastName() {
        final Author author = Author.of("LastName");
        assertEquals("LastName",author.getFullName());
    }

    @Test
    public void testOf() {
        final Author author = Author.of("LastName","FirstName");
        assertEquals("FirstName LastName",author.getFullName());
    }

    @Test
    public void testOf1() {
        final Author author = Author.of("LastName","FirstName","MiddleName");
        assertEquals("FirstName MiddleName LastName",author.getFullName());
    }

    @Test
    public void getDisplayName() {
        final Author author = Author.of("LastName","FirstName","MiddleName");
        assertEquals("FirstName LastName",author.getDisplayName());
    }

    @Test
    public void getDisplayName2() {
        final Author author = Author.of("LastName");
        assertEquals("LastName",author.getDisplayName());
    }

    @Test
    public void getFullName() {
        final Author author = Author.of("LastName","F","M");
        assertEquals("F. M. LastName",author.getFullName());
    }
}