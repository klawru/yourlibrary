package ru.hldns.klaw.web.controller;

import org.springframework.batch.core.JobParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.hldns.klaw.web.service.AdminPageService;

@Controller
@RequestMapping(value = "/admin")
public class AdminPanelController {

    private final AdminPageService service;

    @Autowired
    public AdminPanelController(AdminPageService adminPageService) {
        service = adminPageService;
    }


    @RequestMapping
    public String mainPage() {
        return "admin";
    }


    @RequestMapping("/parseInpxFileJob")
    public String parseInpxFileJob() throws Exception {
        service.runParseInpxJob(new JobParameters());
        return "redirect:/admin";
    }


    @RequestMapping("/parseFB2FilesJob")
    public String parseFB2FilesJob() throws Exception {
        service.runParseFB2Job(new JobParameters());
        return "redirect:/admin";
    }

    @RequestMapping("/cacheEvict")
    public String parseCacheEvict() {
        service.allCacheEvict();
        return "redirect:/admin";
    }

    @RequestMapping("/clearDeletedBook")
    public String clearDeletedBook() {
        service.clearDeletedBook();
        return "redirect:/admin";
    }

}
