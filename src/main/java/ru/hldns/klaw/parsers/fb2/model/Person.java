package ru.hldns.klaw.parsers.fb2.model;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Person {
    String id;
    String firstName;
    String middleName;
    String lastName;
    String nickname;
    ArrayList<String> homePages;
    ArrayList<String> emails;

    public void addHomePage(String elementText) {
        if(homePages==null)
            homePages = new ArrayList<>(4);
        homePages.add(elementText);
    }

    public void addEmail(String elementText) {
        if(emails==null)
            emails = new ArrayList<>(4);
        emails.add(elementText);
    }
}
