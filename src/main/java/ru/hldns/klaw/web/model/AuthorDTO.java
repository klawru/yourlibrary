package ru.hldns.klaw.web.model;

public interface AuthorDTO {
    int getId();

    String getFullName();
}