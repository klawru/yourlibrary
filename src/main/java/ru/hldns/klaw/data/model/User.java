package ru.hldns.klaw.data.model;

import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "eperson")
@Data
@EqualsAndHashCode(of = {"email", "username"})
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eperson_seq")
    @SequenceGenerator(name = "eperson_seq", sequenceName = "eperson_seq", allocationSize = 1)
    @Column(name = "eperson_id", updatable = false, nullable = false)
    @With
    private Integer id;

    @Column(name = "name", length = 20, unique = true)
    private String username;

    @Column(name = "email", length = 254, unique = true)
    private String email;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.DETACH}, fetch = FetchType.EAGER)
    @JoinTable(name = "epersongroup2person",
            joinColumns = @JoinColumn(name = "eperson_id"),
            inverseJoinColumns = @JoinColumn(name = "epersongroup_id"))
    private Set<UserGroup> authorities;

    @Column(name = "password", length = 60)
    private String password;

    @Column(name = "can_log_in")
    private boolean enabled;
    @Column(name = "non_locked")
    private boolean accountNonLocked;
    @Column(name = "non_expired")
    private boolean accountNonExpired;
    @Column(name = "credentials_Non_Expired")
    private boolean isCredentialsNonExpired;

    @Column(name = "oauth")
    private boolean OAuthUser;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Map<String, Object> attributes;

    public void setAuthority(UserGroup authorities) {
        if (this.authorities == null)
            this.authorities = new LinkedHashSet<>();
        this.authorities.add(authorities);
    }
}
