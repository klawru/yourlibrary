package ru.hldns.klaw.web;

public class WebUtility {
    public static int getPageNum(int pageNum) {
        if (pageNum < 1)
            pageNum = 0;
        else
            pageNum -= 1;
        return pageNum;
    }
}
