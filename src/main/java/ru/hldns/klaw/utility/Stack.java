package ru.hldns.klaw.utility;

import lombok.ToString;

import java.util.ArrayList;

@ToString
public class Stack<T> {
    private final ArrayList<T> stack = new ArrayList<>();


    public void push(T e) {
        stack.add(e);
    }

    public T pop() {
        return stack.remove(stack.size() - 1);
    }

    public int size() {
        return stack.size();
    }
}
