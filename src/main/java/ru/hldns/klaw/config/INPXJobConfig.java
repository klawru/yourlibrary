package ru.hldns.klaw.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.model.UpdateInfo;
import ru.hldns.klaw.data.repository.UpdateInfoRepository;
import ru.hldns.klaw.parsers.JobListener;
import ru.hldns.klaw.parsers.inpx.BookItemProcessor;
import ru.hldns.klaw.parsers.inpx.BookItemWriter;
import ru.hldns.klaw.parsers.inpx.InpxItemReader;
import ru.hldns.klaw.parsers.inpx.model.ParserServiceDAO;

import java.time.Instant;

@Slf4j
@Configuration
public class INPXJobConfig {

    @Bean("inpxBookReader")
    public ItemReader<String> getInpxReader(ParserServiceDAO service, ApplicationConfig applicationConfig) {
        InpxItemReader reader = new InpxItemReader();
        reader.setPath(applicationConfig.getPathToInpxFile());
        reader.setService(service);
        return reader;
    }

    @Bean("inpxBookProcessor")
    public ItemProcessor<String, Book> getBookItemProcessor(MessageSource messageSource) {
        return new BookItemProcessor(messageSource);
    }

    @Bean("inpxBookWriter")
    public ItemWriter<Book> writer(ParserServiceDAO service) {
        return new BookItemWriter(service);
    }

    @Bean("parseInpxFileJob")
    public Job parseInpxFileJob(JobBuilderFactory jobs, Step parseInpxStep, JobListener jobListener) {
        return jobs.get("parseInpxFileJob")
                .incrementer(new RunIdIncrementer())
                .flow(parseInpxStep)
                .end()
                .listener(jobListener)
                .build();
    }

    @Bean("parseInpxStep")
    public Step parseInpx(StepBuilderFactory stepBuilderFactory,
                          @Qualifier("inpxBookReader") ItemReader<String> reader,
                          @Qualifier("inpxBookWriter") ItemWriter<Book> writer,
                          @Qualifier("inpxBookProcessor") ItemProcessor<String, Book> processor,
                          PlatformTransactionManager transactionManager,
                          StepExecutionListener updateInfoListener) {
        return stepBuilderFactory.get("parseInpx")
                .<String, Book>chunk(300)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .listener(updateInfoListener)
                .allowStartIfComplete(true)
                .transactionManager(transactionManager)
                .build();
    }

    @Bean
    public StepExecutionListener updateInfoListener(UpdateInfoRepository repository) {
        return new StepExecutionListener() {
            @Override
            public void beforeStep(StepExecution stepExecution) {
                // не нужен
            }

            @Override
            public ExitStatus afterStep(StepExecution stepExecution) {
                final var updateInfo = repository.findById("main")
                        .orElse(new UpdateInfo("main", 0L, Instant.now()));
                updateInfo.setUpdated(Instant.now());
                updateInfo.setBookCount((long) stepExecution.getWriteCount());
                repository.save(updateInfo);
                return null;
            }
        };
    }

}
