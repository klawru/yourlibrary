package ru.hldns.klaw.web.model;

import java.util.Set;

public interface CommunityDTO {
    Integer getId();

    String getName();

    Set<GenreDTO> getGenres();

    interface GenreDTO {
        Integer getId();

        String getName();
    }
}
