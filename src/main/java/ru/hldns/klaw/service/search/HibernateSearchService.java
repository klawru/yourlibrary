package ru.hldns.klaw.service.search;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.search.Query;
import org.hibernate.search.bridge.util.impl.NumericFieldUtils;
import org.hibernate.search.engine.ProjectionConstants;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.search.query.engine.spi.FacetManager;
import org.hibernate.search.query.facet.Facet;
import org.hibernate.search.query.facet.FacetSortOrder;
import org.hibernate.search.query.facet.FacetingRequest;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.config.ApplicationConfig;
import ru.hldns.klaw.web.SearchingResults;
import ru.hldns.klaw.web.controller.SearchController;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static ru.hldns.klaw.web.WebUtility.getPageNum;

@Slf4j
@Repository
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class HibernateSearchService implements ApplicationListener<ApplicationReadyEvent> {

    private final boolean disableSearchService;

    private EntityManager entityManager;

    public HibernateSearchService(ApplicationConfig applicationConfig) {
        this.disableSearchService = applicationConfig.isDisableSearch();
    }

    private static void setFiltersOnQuery(SearchController.SearchForm.Filter[] filters, BooleanJunction<?> mainQuery, QueryBuilder qb) {
        for (var filter : filters) {
            if (filter.getValue() == null || filter.getValue().isEmpty())//Игнорируем пустой фильтр
                continue;
            Query temp = null;
            switch (filter.getField()) {
                case title:
                    temp = qb.phrase().onField("title").boostedTo(6).sentence(filter.getValue()).createQuery();
                    break;
                case author:
                    temp = qb.phrase().onField("authors.fullName").boostedTo(6).sentence(filter.getValue()).createQuery();
                    break;
                case series:
                    temp = qb.phrase().onField("series.name").boostedTo(6).sentence(filter.getValue()).createQuery();
                    break;
                case issued:
                    temp = qb.keyword().onFields("date").boostedTo(6).matching(filter.getValue()).createQuery();
                    break;
                case lang:
                    temp = qb.keyword().onFields("lang").boostedTo(6).matching(filter.getValue().toLowerCase()).createQuery();
                    break;
            }
            switch (filter.getFilter()) {
                case should:
                    mainQuery = mainQuery.should(temp);
                    break;
                case equals:
                    mainQuery = mainQuery.must(temp);
                    break;
                case nonEquals:
                    mainQuery = mainQuery.must(temp).not();
                    break;
            }
        }
    }

    private static List<String> tokenizeString(Analyzer analyzer, String string) {
        List<String> result = new ArrayList<>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
            stream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private static QueryBuilder getQueryBuilderFor(@SuppressWarnings("SameParameterValue") Class<?> entity, FullTextEntityManager manager) {
        return manager.getSearchFactory().buildQueryBuilder().forEntity(entity).get();
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public SearchingResults mainSearch(String searchTerm, int communityId, SearchController.SearchForm.Filter[] filters, int page, int limit) {
        int firstResult = getPageNum(page);
        FullTextEntityManager manager = getManager();
        QueryBuilder qb = getQueryBuilderFor(Book.class, manager);

        var query = qb.bool();

        query.must(qb.simpleQueryString().onFields("authors.fullName", "keyword", "series.name").andField("title").boostedTo(3f).matching(searchTerm).createQuery());
        if (communityId != 0)
            query.must(NumericFieldUtils.createExactMatchQuery("genres.community.communityId", communityId));
        //add filter query to main query
        setFiltersOnQuery(filters, query, qb);

        SearchingResults result = new SearchingResults();
        result.setCurrentPage(page);
        try {
            //receive result projection: ID,"title","authors", "genres.community.name"
            FullTextQuery jpaQuery = addResultProjection(firstResult, limit, manager, query);

            //Disable faceting on community, if the search is for a specific community
            if (communityId == 0)
                jpaQuery.getFacetManager().enableFaceting(getCommunityFaceting(qb));
            jpaQuery.getFacetManager().enableFaceting(getAuthorFacet(qb));

            jpaQuery.limitExecutionTimeTo(500, TimeUnit.MILLISECONDS);
            // execute search
            result.setResultList(jpaQuery.getResultList());
            result.setAuthorsFacet(jpaQuery.getFacetManager().getFacets("authorFacet"));

            if (communityId == 0)
                result.setCommunityFacet(jpaQuery.getFacetManager().getFacets("communityFacetId"));
            result.setResultSize(jpaQuery.getResultSize());
            result.setHasPartialResults(jpaQuery.hasPartialResults());
        } catch (NoResultException nre) {
            result.setResultList(Collections.emptyList());
        } catch (Exception e) {
            log.error("Error while search searchTerm='{}',communityId='{}',filters='{}', firstresult='{}', maxresult='{}', error='{}'", searchTerm, communityId, filters, firstResult, limit, e);
        }
        return result;
    }

    private FullTextQuery addResultProjection(int firstResult, int maxResult, FullTextEntityManager manager, BooleanJunction<?> query) {
        return manager.createFullTextQuery(query.createQuery(), Book.class)
                .setFirstResult(firstResult).setMaxResults(maxResult)
                .setProjection(ProjectionConstants.ID, "title", "authors", "genres.community.name", "series.name", "displayFileSize", "date", "lang", "coverPage");
    }

    private FacetingRequest getCommunityFaceting(QueryBuilder qb) {
        return qb.facet()
                .name("communityFacetId")
                .onField("genres.community.communityFacetId")
                .discrete()
                .orderedBy(FacetSortOrder.COUNT_DESC)
                .maxFacetCount(10)
                .createFacetingRequest();
    }

    private FacetingRequest getAuthorFacet(QueryBuilder qb) {
        return qb.facet()
                .name("authorFacet")
                .onField("authors.authorFacet")
                .discrete()
                .orderedBy(FacetSortOrder.COUNT_DESC)
                .includeZeroCounts(false)
                .maxFacetCount(10)
                .createFacetingRequest();
    }

    public List<Facet> topAuthorsByCommunity(int communityId) {
        FullTextEntityManager fullTextEntityManager = getManager();
        QueryBuilder qb = getQueryBuilderFor(Book.class, fullTextEntityManager);
        final var query = qb.bool().must(
                NumericFieldUtils.createExactMatchQuery("genres.community.communityId", communityId)).createQuery();
        FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, Book.class);
        FacetManager facetManager = fullTextQuery.getFacetManager();
        facetManager.enableFaceting(getAuthorFacet(qb));
        return facetManager.getFacets("authorFacet");
    }

    private FullTextEntityManager getManager() {
        return Search.getFullTextEntityManager(entityManager);
    }

    /**
     * Производит иницилизацию поиска после запуска приложения
     */
    @Override
    @SneakyThrows
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        try {
            if (!disableSearchService) {
                FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
                fullTextEntityManager.createIndexer().startAndWait();
            }
        } catch (InterruptedException e) {
            log.error("On create index", e);
            throw e;
        }
    }

}