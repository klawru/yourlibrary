package ru.hldns.klaw.web.model;

public interface CommunityDTOMin {
    Integer getId();

    String getName();
}
