package ru.hldns.klaw.parsers.fb2.model;


import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "value")
public class Binary {
    protected byte[] value;
    protected String contentType;
    protected String id;
    protected String hash;
}