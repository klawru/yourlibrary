package ru.hldns.klaw.service;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.User;
import ru.hldns.klaw.data.model.UserGroup;
import ru.hldns.klaw.data.repository.UserGroupRepository;
import ru.hldns.klaw.data.repository.UserRepository;

import java.util.HashSet;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final UserGroupRepository userGroupRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       UserGroupRepository userGroupRepository,
                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userGroupRepository = userGroupRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(@NonNull String email) throws UsernameNotFoundException {
        //Users authorized through OAuth should not be returned by this method.
        var user = userRepository.findByEmailAndOAuthUser(email.toLowerCase(), false);
        return user.orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public Optional<User> loadByName(@NonNull String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    public Optional<User> loadUserByEmail(@NonNull String email, boolean isOAuth) throws UsernameNotFoundException {
        return userRepository.findByEmailAndOAuthUser(email, isOAuth);
    }

    @Transactional
    public void saveUser(@NonNull User user) {
        user.setEmail(user.getEmail().toLowerCase());
        if (!user.isOAuthUser())
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        refreshUserGroupFromDB(user);
        userRepository.save(user);
    }

    private void refreshUserGroupFromDB(@NonNull User user) {
        if (user.getAuthorities() == null) {
            userGroupRepository.findByAuthority("ROLE_USER").ifPresent(user::setAuthority);
        } else {
            final var userGroups = new HashSet<UserGroup>();
            //todo переделать
            for (UserGroup userGroup : user.getAuthorities()) {
                if (userGroup.getId() == null) {
                    userGroups.add(userGroupRepository.findByAuthority(userGroup.getAuthority()).orElse(userGroup));
                } else {
                    userGroups.add(userGroup);
                }
            }
            user.setAuthorities(userGroups);
        }
    }

    public void changePassword(User user, String newPassword) {
        user.setPassword(newPassword);
        saveUser(user);
    }

    public boolean validatePassword(User user, String password) {
        final var hashPassword = user.getPassword();
        return passwordEncoder.matches(password, hashPassword);
    }

    public boolean existsByUsername(String name) {
        return userRepository.existsByUsername(name);
    }

    public boolean existsByEmail(String name) {
        return userRepository.existsByEmail(name);
    }

}
