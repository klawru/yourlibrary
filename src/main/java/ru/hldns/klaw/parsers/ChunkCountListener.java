package ru.hldns.klaw.parsers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.scope.context.ChunkContext;

@Slf4j
public class ChunkCountListener implements ChunkListener {
    private String itemName = "{} items processed";
    private final int loggingInterval;

    public ChunkCountListener(String itemName,int loggingInterval) {
        this.loggingInterval = loggingInterval;
        setItemName(itemName);
    }

    @Override
    public void beforeChunk(ChunkContext context) {
        // Nothing to do here
    }

    @Override
    public void afterChunk(ChunkContext context) {
        int count = context.getStepContext().getStepExecution().getReadCount();
        if (count > 0 && count % loggingInterval == 0) {
            log.info(itemName, count);
        }
    }

    @Override
    public void afterChunkError(ChunkContext context) {
        // Nothing to do here
    }

    public void setItemName(String itemName) {
        this.itemName = "{} " + itemName + " processed";
    }
}
