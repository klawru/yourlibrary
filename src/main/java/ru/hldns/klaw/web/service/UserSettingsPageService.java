package ru.hldns.klaw.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import ru.hldns.klaw.data.model.User;
import ru.hldns.klaw.service.UserService;
import ru.hldns.klaw.web.errors.NotFoundUserException;

import java.util.Optional;

@Service
public class UserSettingsPageService {
    private final UserService userService;
    private final MessageSource messageSource;


    @Autowired
    public UserSettingsPageService(UserService userService, MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    public void changeUserPassword(String name, String currentPassword, String newPassword, BindingResult result, Model model) throws NotFoundUserException {
        Optional<User> optionalUser = userService.loadByName(name);
        if (optionalUser.isPresent())
            if (userService.validatePassword(optionalUser.get(), currentPassword)) {
                userService.changePassword(optionalUser.get(), newPassword);
                model.addAttribute("success", messageSource.getMessage("settings.passwordChanged", null, LocaleContextHolder.getLocale()));
            } else
                result.addError(new FieldError("passwordForm", "currentPassword", "err.PasswordIncorrect"));
        else
            throw new NotFoundUserException("User with this name '" + name + "' not found. Unable to change password.");
    }

}
