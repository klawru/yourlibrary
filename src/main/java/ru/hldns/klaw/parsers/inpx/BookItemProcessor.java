package ru.hldns.klaw.parsers.inpx;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ParseException;
import org.springframework.context.MessageSource;
import ru.hldns.klaw.data.model.Author;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.model.Genre;
import ru.hldns.klaw.data.model.Series;
import ru.hldns.klaw.utility.FileUtility;
import ru.hldns.klaw.data.utility.MetaDataField;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * Parse string from record from INP files to Entity Book.
 */
@Slf4j
public class BookItemProcessor implements ItemProcessor<String, Book> {
    private static final String MAIN_DELIMITER = String.valueOf((char) 4);
    private static final String DELIMITER = ":";
    private final String authorUnknown;
    private final String untitled;

    public BookItemProcessor(MessageSource messageSource) {
        authorUnknown = messageSource.getMessage("author.unknown", null, Locale.getDefault());
        untitled = messageSource.getMessage("untitled", null, Locale.getDefault());
    }

    //0)AUTHOR; 1)GENRE; 2)TITLE; 3)SERIES; 4)SERIES №; 5)FILE; 6)SIZE; 7)LIB ID; 8)DEL; 9)EXT; 10)DATE; 11)LANG; 12)LIBRARY RATE; 13)KEYWORDS; + 14)Folder(name Zipfile added from reader)
    //Exaple string: Роулинг,Джоан,:sf_fantasy:Harry Potter e il prigioniero di AzkabanHarry Potter (it)31958657482091958650fb22010-08-02itHarry Potter and the Prisoner of Azkaban
    @Override
    public Book process(String inpEntryString) {
        //Данные  в строке разделены символом \u0004
        String[] parts = inpEntryString.split(MAIN_DELIMITER);
        if (parts.length != 15)
            throw new ParseException("INP entry has only " + parts.length + " parts. Necessary 15 parts. String:" + inpEntryString);

        Book book = new Book();
        parseAuthors(book, parts[0]);   //0)AUTHOR
        parseGenres(book, parts[1]);    //1)GENRE
        parseTitle(book, parts[2]);        //2)TITLE
        parseSeries(book, parts[3], parts[4]);       //3)SERIES
        parseFile(book, parts[5]);      //5)FILE
        parseFileSize(book, parts[6]);  //6)SIZE
        parseLibId(book, parts[7]);     //7)LIBID;
        parseDeletePart(book, parts[8]); //8)DEL
        parseFileExtension(book, parts[9]);//9)EXT
        parseDate(book, parts[10]);     //10)DATE
        parseLang(book, parts[11]);     //11)LANG
        parseKeyword(book, parts[13]);  //13)KEYWORDS
        book.setFolder(parts[14]);      //14) Folder from InpxItemReader
        return book;
    }

    private void parseTitle(Book book, String part) {
        final var title = part.strip();
        if (title.length() < 250)
            if (title.isBlank()) {
                book.setTitle(untitled);
            } else
                book.setTitle(title);
        else {
            book.setTitle(title.substring(0, 246) + "...");
            book.addMetadata(MetaDataField.TITLE, part);
            log.info("Title too long:\"{}\" in the book {}", title, book);
        }
    }

    private void parseKeyword(Book book, String part) {
        if (part.length() > 0)
            book.addMetadata(MetaDataField.KEYWORD, part);
    }

    private void parseLang(Book book, String part) {
        //remove all non-alphabetical characters
        var lang = part.replaceAll("\\W", "").toLowerCase();
        if (lang.isBlank())
            book.setLang("und");//Undetermined
        else if (lang.length() < 5)//constraint on column
            book.setLang(lang.toLowerCase());
        else {
            book.setLang(lang.substring(0, 2));
            book.addMetadata(MetaDataField.LANG, lang);
        }
    }

    /**
     * LibId - Book id in library Likely to be unique across one .inpx file.
     *
     * @param book results is being save to this object.
     * @param part should number.
     */
    private void parseLibId(Book book, String part) {
        try {
            book.setLibId(Integer.parseInt(part));
        } catch (NumberFormatException e) {
            log.info("NumberFormatException for \"LibId\":'{}' in book {}", part, book);
        }
    }

    /**
     * @param book results are saved to this object.
     * @param part Empty - book is present. 1 - book is deleted.
     */
    private void parseDeletePart(Book book, String part) {
        if (!(part.equals("0") || part.isBlank()))
            book.setDeleted(true);
    }

    private void parseFileSize(Book book, String part) {
        try {
            final var size = Integer.parseInt(part);
            book.setFileSize(size);
            book.setDisplayFileSize(FileUtility.bytesToString(size));
        } catch (NumberFormatException e) {
            log.info("NumberFormatException for \"File Size\":'{}' in book {}", part, book);
        }
    }

    private void parseFileExtension(Book book, String part) {
        book.setFileExtension(part);
    }

    private void parseFile(Book book, String part) {
        if (part.length() < 255)
            book.setFile(part);
        else {
            log.info("Filename are longer than should. Add to metadata as text {}", part);
            book.addMetadata(MetaDataField.FILE_NAME, part);
        }
    }

    /**
     * Parse 10)DATE exp:2010-08-02
     *
     * @param book results are saved to this object.
     * @param part Should be in format YYYY-MM-DD.
     */
    private void parseDate(Book book, String part) {
        try {
            book.setDate(LocalDate.parse(part));
        } catch (DateTimeParseException e) {
            book.addMetadata(MetaDataField.ISSUED, part);
        }
    }

    /**
     * Parse 3)SERIES; 4)SERNO;
     *
     * @param book results are saved to this object.
     */
    private void parseSeries(Book book, String series, String number) {
        if (!series.isBlank()) {
            book.setSeries(new Series(series.strip()));
            //parse SERIES number
            if (!number.isBlank())
                try {
                    book.setSeriesNumber(Integer.parseInt(number));
                } catch (NumberFormatException e) {
                    log.info("NumberFormatException for \"series number\":'{}'. Add to metadata as text. Book:{}", number, book.toString());
                    book.addMetadata(MetaDataField.SERIES_NUMBER, number);
                }

        }
    }

    private void parseGenres(Book book, String part) {
        //Пример строки "sf_fantasy:"
        String[] genres = part.split(DELIMITER);
        Set<Genre> genreSet = new HashSet<>(genres.length);
        for (String genreName : genres) {
            if (genreName.length() > 0) {
                Genre genre = Genre.of(genreName);
                genreSet.add(genre);
            }
        }
        book.setGenres(genreSet);
    }

    private void parseAuthors(Book book, String part) {
        //Пример строки "Роулинг,Джоан,:"
        String[] authors = part.split(DELIMITER);
        for (String author : authors) {
            if (author.isBlank())
                continue;
            String[] nameParts = author.split(",(?!\\s)");
            if (nameParts.length > 3) {
                //Если получили >3 элементов. Возможно часть элементов пустые после метода split
                List<String> namesList = new ArrayList<>(6);
                for (String namePart : nameParts) {
                    if (!namePart.isBlank())
                        namesList.add(namePart);
                }
                nameParts = namesList.toArray(String[]::new);
            }

            Author authorName = null;
            switch (nameParts.length) {
                case 0:
                    break;
                default:
                    //>3? Просто сохряняем все это в LastName
                    authorName = Author.of(String.join(" ", nameParts));
                    break;
                case 3:
                    authorName = Author.of(nameParts[0], nameParts[1], nameParts[2]);
                    break;
                case 2:
                    authorName = Author.of(nameParts[0], nameParts[1]);
                    break;
                case 1:
                    authorName = Author.of(nameParts[0]);
                    break;
            }
            if (authorName != null)
                book.addAuthors(authorName);
        }
        if (book.getAuthors() == null || book.getAuthors().size() == 0) {
            book.addAuthors(Author.of(authorUnknown));
        }
    }
}
