package ru.hldns.klaw.data.model;

import lombok.*;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.IndexedEmbedded;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "genres")
@Data
@EqualsAndHashCode(of = {"genre"})
@ToString(of = {"id", "name"})
@NoArgsConstructor
@AllArgsConstructor
public class Genre implements Serializable {
    @Id
    @Column(name = "id_genre", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @With
    Integer id;

    @Field(analyze = Analyze.NO)
    @Column(name = "name", nullable = false)
    @Size(min = 1, max = 150)
    String name;

    static public Genre of(String genre, Community community) {
        return new Genre(null,genre,"",genre,null,community);
    }

    static public Genre of(String genre) {
        return new Genre(null,genre,"",genre,null,null);
    }

    @Column(name = "abstract")
    @Size(min = 1, max = 500)
    String description;

    /**
     * FB2 genre
     */
    @Column(name = "genre", nullable = false, unique = true)
    @Size(min = 1, max = 150)
    String genre;

    @ContainedIn
    @ManyToMany(mappedBy = "genres")
    private Set<Book> books;

    @IndexedEmbedded
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_community")
    private Community community;

}
