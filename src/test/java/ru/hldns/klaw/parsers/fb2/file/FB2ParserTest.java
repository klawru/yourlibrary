package ru.hldns.klaw.parsers.fb2.file;

import com.fasterxml.aalto.stax.InputFactoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;

public class FB2ParserTest {
    private InputFactoryImpl xmlInputFactory;
    private ClassLoader classLoader;
    @BeforeEach
    public void setUp() {
        classLoader = getClass().getClassLoader();
        xmlInputFactory = new InputFactoryImpl();
    }

    @Test
    public void parse() throws Exception {
        InputStream inputStream = classLoader.getResourceAsStream("1.fb2");
        try (StaxStreamProcessor processor = new StaxStreamProcessor(inputStream, xmlInputFactory)) {
            final var description = FB2Parser.parse(processor);

            assertThat(description,hasProperty("titleInfo"));
            assertThat(description,hasProperty("srcTitleInfo"));
            assertThat(description,hasProperty("documentInfo"));
            assertThat(description,hasProperty("publishInfo"));

            final var titleInfo = description.getTitleInfo();
            assertThat(titleInfo,hasProperty("annotation"));
            assertThat(titleInfo,hasProperty("genre"));
            assertThat(titleInfo,hasProperty("author"));
            assertThat(titleInfo,hasProperty("bookTitle"));
            assertThat(titleInfo,hasProperty("coverPage"));
            assertThat(titleInfo,hasProperty("lang"));
        }
    }

    @Test
    public void parseStopAfterDescription() throws Exception {
        InputStream inputStream = classLoader.getResourceAsStream("2.fb2");
        try (StaxStreamProcessor processor = new StaxStreamProcessor(inputStream, xmlInputFactory)) {
            final var parse = FB2Parser.parse(processor);
            assertThat(processor.getReader().hasNext(), is(true));
        }
    }
}