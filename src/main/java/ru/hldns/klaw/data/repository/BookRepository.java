package ru.hldns.klaw.data.repository;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.model.Genre;
import ru.hldns.klaw.web.model.BookDTO;
import ru.hldns.klaw.web.model.BookDTOMin;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


public interface BookRepository extends JpaRepository<Book, Integer> {

    Page<Book> findAllByGenres(Genre genre, Pageable page);

    List<Book> findAllByLibIdIn(Collection<Integer> collection);

    Page<BookDTOMin> findAllByAuthorsId(int id, Pageable pageable);

    @Query(value = "SELECT b.id_book as id, b.name as title, s.name as series, b.series_number as seriesNumber, " +
            "       b.cover_page as coverPage, b.date as date, b.lang as lang, b.file_size_string as displayFileSize, " +
            "       (SELECT string_agg(a.name, ', ') as authors " +
            "        FROM lateral (SELECT a.first_name || ' ' || a.last_name as name " +
            "                      FROM authors a " +
            "                               JOIN books2authors b2a on a.id_author = b2a.id_author " +
            "                      WHERE b2a.id_book = b.id_book " +
            "                      limit 5) as a), " +
            "       (SELECT string_agg(g.name, ', ') as genres " +
            "          FROM genres g " +
            "                   JOIN genres2books g2b on g.id_genre = g2b.id_genre and g2b.id_book = b.id_book " +
            "          GROUP BY b.id_book ) " +
            "FROM books as b " +
            "         LEFT JOIN series s on b.series_id_series = s.id_series " +
            "         INNER  JOIN (SELECT g2b2.id_book FROM genres2books AS g2b2 WHERE g2b2.id_genre = ?1 ORDER BY g2b2.id_book LIMIT ?2 OFFSET ?3) " +
            "               as g2b ON g2b.id_book = b.id_book " +
            "WHERE b.deleted<>true",
            nativeQuery = true)
    List<BookDTO> findByGenre(int genre, int limit, long offset);

    @Cacheable(cacheNames = "countBookByGenre")
    @Query(value = "SELECT count(*) FROM genres2books WHERE genres2books.id_genre = ?1", nativeQuery = true)
    Integer countBookByGenresIs(int genre);

    @Override
    @EntityGraph(attributePaths = "authors", type = EntityGraph.EntityGraphType.LOAD)
    Optional<Book> findById(Integer integer);

    <T> List<T> findBySeriesNameOrderByTitle(String name, Class<T> projection);

    Page<Book> findAllByDeletedFalse(Pageable page);

    Page<Book> findAllByDeletedFalseAndCoverPageIsNull(Pageable page);

    void deleteAllByDeletedTrue();
}
