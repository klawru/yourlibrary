package ru.hldns.klaw.web.service;

import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class AutoUpdateService {
    private final JobLauncher jobLauncher;
    private final Job autoUpdateJob;

    public AutoUpdateService(@Qualifier("asyncJobLauncher") JobLauncher jobLauncher, Job autoUpdateJob) {
        this.jobLauncher = jobLauncher;
        this.autoUpdateJob = autoUpdateJob;
    }

    //Каждый пятый день месяца в 12:00
    @Scheduled(cron = "0 0 12 5 * *")
    @SneakyThrows
    public void triggerAutoUpdate() {
        jobLauncher.run(autoUpdateJob, new JobParameters());
    }
}
