package ru.hldns.klaw.web.results;

import lombok.Value;

@Value
public class Result<T, E> {
    private T mainObject;
    private E results;

    private Result(T object, E results) {
        this.mainObject = object;
        this.results = results;
    }

    public static <T, E> Result<T, E> of(T object, E results) {
        return new Result<>(object, results);
    }


    private static final Result<?, ?> EMPTY = new Result<>(null, null);

    public static <T, E> Result<T, E> empty() {
        @SuppressWarnings("unchecked") final Result<T, E> t = (Result<T, E>) EMPTY;
        return t;
    }

    public boolean isEmpty() {
        return mainObject == null;
    }
}