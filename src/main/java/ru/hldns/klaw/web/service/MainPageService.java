package ru.hldns.klaw.web.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.UpdateInfo;
import ru.hldns.klaw.data.repository.*;
import ru.hldns.klaw.utility.Pair;
import ru.hldns.klaw.web.model.CommunityDTOMin;
import ru.hldns.klaw.web.model.CountingResult;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@Service
public class MainPageService {

    private final CommunityRepository communityRepository;
    private final BookRepository bookRepository;
    private final AuthorsRepository authorsRepository;
    private final BookSeriesRepository seriesRepository;
    private final MessageSource messageSource;
    private final UpdateInfoRepository updateInfoRepository;

    @Autowired
    public MainPageService(CommunityRepository communityRepository, BookRepository bookRepository,
                           AuthorsRepository authorsRepository, BookSeriesRepository seriesRepository,
                           MessageSource messageSource, UpdateInfoRepository updateInfoRepository) {
        this.communityRepository = communityRepository;
        this.bookRepository = bookRepository;
        this.authorsRepository = authorsRepository;
        this.seriesRepository = seriesRepository;
        this.messageSource = messageSource;
        this.updateInfoRepository = updateInfoRepository;
    }

    private List<CountingResult> getTopAuthors() {
        return authorsRepository.getTopByCountBooks(10);
    }

    @Cacheable("MainPageResult")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public MainPageResult getMainPageData() {
        final Set<CommunityDTOMin> communities = communityRepository.findBy(CommunityDTOMin.class);
        ArrayList<CommunityDTOMin> communityList = new ArrayList<>(communities);
        communityList.sort(Comparator.comparing(CommunityDTOMin::getName));

        final long countBooks = bookRepository.count();
        final long countAuthor = authorsRepository.count();
        final long countSeries = seriesRepository.count();
        final List<CountingResult> topAuthors = getTopAuthors();
        final Pair<String[], Long[]> countBookByCommunity = getCountBookByCommunity(10);
        final var updateInfo = updateInfoRepository.findById("main")
                .orElse(new UpdateInfo("main", 0L, Instant.now()));
        return new MainPageResult(communityList,
                topAuthors,
                countBooks,
                countAuthor,
                countSeries,
                countBookByCommunity.getFirst(),
                countBookByCommunity.getSecond(),
                updateInfo);
    }

    @AllArgsConstructor
    @Getter
    public static class MainPageResult {
        List<CommunityDTOMin> communities;
        List<CountingResult> topAuthors;
        long countBooks;
        long countAuthor;
        long countSeries;
        String[] countBookCommName;
        Long[] countBookComm;
        UpdateInfo updateInfo;
    }

    private Pair<String[], Long[]> getCountBookByCommunity(int maximum) {
        //Форматирование данных для диаграммы из 10 элементов
        final List<CountingResult> countBookByCommunity = communityRepository.countBookByCommunity();
        final var sizeArray = countBookByCommunity.size();
        String[] countBookCommName;
        Long[] countBookComm;
        if (sizeArray < maximum) {
            countBookCommName = new String[sizeArray];
            countBookComm = new Long[sizeArray];
            for (int i = 0; i < sizeArray; i++) {
                countBookCommName[i] = countBookByCommunity.get(i).getResult();
                countBookComm[i] = countBookByCommunity.get(i).getCount();
            }
        } else {
            //Сумируем последние элементы
            final long sumLastElements = countBookByCommunity.subList(maximum - 1, sizeArray).stream().mapToLong(CountingResult::getCount).sum();
            // Оставляем 9 результатов, и добавляем сумму последних элементов
            countBookCommName = new String[maximum];
            countBookComm = new Long[maximum];
            for (int i = 0; i < maximum - 1; i++) {
                countBookCommName[i] = countBookByCommunity.get(i).getResult();
                countBookComm[i] = countBookByCommunity.get(i).getCount();
            }
            countBookCommName[maximum - 1] = messageSource.getMessage("main.allOtherGenres", null, LocaleContextHolder.getLocale());
            countBookComm[maximum - 1] = sumLastElements;
        }
        return new Pair<>(countBookCommName, countBookComm);
    }

}
