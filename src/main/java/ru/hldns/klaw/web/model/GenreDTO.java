package ru.hldns.klaw.web.model;

public interface GenreDTO {
    Integer getId();

    String getName();
}
