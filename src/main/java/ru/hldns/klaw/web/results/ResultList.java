package ru.hldns.klaw.web.results;

import lombok.Value;
import org.springframework.data.domain.Page;
import ru.hldns.klaw.utility.Pagination;

@Value
public class ResultList<E, T> {
    private E object;
    private T list;
    private Pagination pagination;

    private ResultList(E main, T results, Pagination pagination) {
        this.object = main;
        this.list = results;
        this.pagination = pagination;
    }

    public static <E, T> ResultList<E, T> of(E main, T results, Pagination pagination) {
        return new ResultList<>(main, results, pagination);
    }

    public static <E, T> ResultList<E, Page<T>> of(E result, Page<T> list, int paginationLength) {
        return new ResultList<>(result, list, Pagination.of(list, paginationLength));
    }


    private static final ResultList<?, ?> EMPTY = new ResultList<>(null, null, null);

    public static <E, T> ResultList<E, T> empty() {
        @SuppressWarnings("unchecked") final ResultList<E, T> t = (ResultList<E, T>) EMPTY;
        return t;
    }

    public boolean isEmpty() {
        return object == null;
    }
}
