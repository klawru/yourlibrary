package ru.hldns.klaw.parsers.inpx.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParsedFileRepository extends JpaRepository<ParsedFile, String> {
}
