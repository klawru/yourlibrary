package ru.hldns.klaw.parsers.fb2.model;

import lombok.Data;

@Data
public class SeriesBook {
    String name;
    String number;
    String lang;
}
