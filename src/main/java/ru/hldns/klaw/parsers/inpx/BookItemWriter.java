package ru.hldns.klaw.parsers.inpx;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.Author;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.model.Genre;
import ru.hldns.klaw.data.model.Series;
import ru.hldns.klaw.parsers.inpx.model.ParserServiceDAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class BookItemWriter implements ItemStreamWriter<Book> {
    private static ParserServiceDAO service;

    @PersistenceContext
    EntityManager entityManager;

    private Cache<String, Genre> genreCache;
    private Cache<String, Author> authorsCache;
    private Cache<String, Series> seriesCache;

    @Override
    public void write(List<? extends Book> books) {
        try {
            for (Book book : books) {
                //Save new author, or get saved from DB
                book.setAuthors(findAuthorOrSaveFrom(book.getAuthors()));
                book.setGenres(findGenre(book.getGenres()));
                book.setSeries(findSeriesOrSave(book));
            }
            service.saveItems(books);
            entityManager.flush();
            entityManager.clear();
        } catch (Exception e) {
            log.error("Exception on write books in job", e);
            throw e;
        }
    }

    private Series findSeriesOrSave(final Book book) {
        final var series = book.getSeries();
        if (series != null) {
            var cacheSeries = seriesCache.getIfPresent(series.getName());
            if (cacheSeries == null) {
                final var seriesOptional = findSeries(series);
                var existSeries = seriesOptional.orElseGet(() -> saveSeries(series));
                seriesCache.put(series.name, existSeries);
                return existSeries;
            }
            return cacheSeries;
        }
        return null;
    }

    private Optional<Series> findSeries(Series series) {
        return service.findBookSeries(series);
    }

    private synchronized Series saveSeries(Series series) {
        return service.saveBookSeries(series);
    }

    private Set<Genre> findGenre(Set<Genre> genresFromBook) {
        Set<Genre> returnSet = new HashSet<>(genresFromBook.size());
        for (Genre next : genresFromBook) {
            final var genre = genreCache.getIfPresent(next.getGenre());
            if (genre != null)
                returnSet.add(genreCache.getIfPresent(next.getGenre()));
        }
        return returnSet;
    }

    private Set<Author> findAuthorOrSaveFrom(Set<Author> authors) {
        HashSet<Author> set = new HashSet<>();
        for (Author next : authors) {
            Author author = authorsCache.getIfPresent(next.getFullName());
            if (author == null) {
                author = findAuthor(next.getFullName()).orElseGet(() -> saveAuthor(next));
                authorsCache.put(author.getFullName(), author);
            }
            set.add(author);
        }
        return set;

    }

    private Optional<Author> findAuthor(String next) {
        return service.findAuthor(next);
    }

    private Author saveAuthor(Author next) {
        return service.saveAuthor(next);
    }

    public BookItemWriter(ParserServiceDAO service) {
        BookItemWriter.service = service;
    }


    @Override
    @Transactional
    public void open(ExecutionContext executionContext) throws ItemStreamException {

        seriesCache = Caffeine.newBuilder()
                .maximumSize(5000)
                .build();
        authorsCache = Caffeine.newBuilder()
                .maximumSize(5000)
                .build();
        genreCache = Caffeine.newBuilder()
                .maximumSize(5000)
                .build();

        List<Genre> genreList = service.getAllGenres();
        for (Genre genre : genreList) {
            genreCache.put(genre.getGenre(), genre);
        }
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {
        if (genreCache != null) genreCache.cleanUp();
        if (authorsCache != null) authorsCache.cleanUp();
        if (seriesCache != null) seriesCache.cleanUp();
        genreCache = null;
        authorsCache = null;
        seriesCache = null;
    }
}