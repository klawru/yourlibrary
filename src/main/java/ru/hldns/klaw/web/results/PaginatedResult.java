package ru.hldns.klaw.web.results;

import lombok.Value;
import org.springframework.data.domain.Page;
import ru.hldns.klaw.utility.Pagination;

@Value
public class PaginatedResult<E>{
    private E result;
    private Pagination pagination;

    private PaginatedResult(E main, Pagination pagination) {
        this.result = main;
        this.pagination = pagination;
    }

    public static <E> PaginatedResult<E> of(E main, Pagination pagination) {
        return new PaginatedResult<>(main, pagination);
    }

    public static <T> PaginatedResult<Page<T>> of(Page<T> main, int paginationLength) {
        return new PaginatedResult<>(main, Pagination.of(main, paginationLength));
    }


    private static final PaginatedResult<?> EMPTY = new PaginatedResult<>(null, null);

    public static <T> PaginatedResult<T> empty() {
        @SuppressWarnings("unchecked") final PaginatedResult<T> t = (PaginatedResult<T>) EMPTY;
        return t;
    }

    public boolean isEmpty() {
        return result == null;
    }
}
