package ru.hldns.klaw.data.repository;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.hldns.klaw.data.model.Genre;
import ru.hldns.klaw.web.model.GenreDTOMin;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface GenreRepository extends JpaRepository<Genre, Integer> {

    Set<Genre> findByGenreIn(Collection<String> genre);

    @EntityGraph(attributePaths = {"community"})
    List<Genre> findAll();

    Optional<Genre> findByGenreIs(String genre);

    @Cacheable(cacheNames = "Genre")
    Optional<GenreDTOMin> getById(int id);

    <T> List<T> findByCommunityId(Integer id, Class<T> projection);
}
