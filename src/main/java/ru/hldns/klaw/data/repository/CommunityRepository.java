package ru.hldns.klaw.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.hldns.klaw.data.model.Community;
import ru.hldns.klaw.web.model.CountingResult;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CommunityRepository extends JpaRepository<Community, Integer> {

    <T> Optional<T> findById(Integer integer, Class<T> DTO);

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query(value = "SELECT c.name as result, count(*) as count FROM  community c " +
            "JOIN  genres g on c.id_community = g.id_community " +
            "JOIN  genres2books g2b on g.id_genre = g2b.id_genre " +
            "GROUP BY c.name " +
            "ORDER BY count(*) desc", nativeQuery = true)
    List<CountingResult> countBookByCommunity();

    <T> Set<T> findBy(Class<T> DTO);
}
