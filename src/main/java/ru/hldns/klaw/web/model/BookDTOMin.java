package ru.hldns.klaw.web.model;

import java.time.LocalDate;

public interface BookDTOMin {
    Integer getId();

    String getTitle();

    SeriesDTO getSeries();

    int getSeriesNumber();

    String getCoverPage();

    LocalDate getDate();

    String getLang();

    String getDisplayFileSize();

    interface SeriesDTO {
        int getId();

        String getName();

    }

    default boolean hasCoverPage(){
        return getCoverPage()!=null;
    }

    default boolean hasSeries(){
        return getSeries()!=null;
    }
}
