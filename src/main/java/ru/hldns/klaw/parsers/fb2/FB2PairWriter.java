package ru.hldns.klaw.parsers.fb2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.parsers.inpx.model.ParserServiceDAO;

import java.util.List;

@Slf4j
public class FB2PairWriter implements ItemWriter<Book> {
    private final ParserServiceDAO repository;

    public FB2PairWriter(ParserServiceDAO repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public void write(List<? extends Book> items) {
        repository.saveItems(items);
    }
}
