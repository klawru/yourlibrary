package ru.hldns.klaw.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.hldns.klaw.data.model.Series;

import java.util.Optional;


public interface BookSeriesRepository extends JpaRepository<Series, Integer> {

    Optional<Series> findByNameIs(String name);

    @EntityGraph(value = "series-books",type = EntityGraph.EntityGraphType.LOAD)
    Optional<Series> findByName(String name);

    @Query("SELECT s FROM  Series s WHERE FUNCTION('f_unaccent', s.name) like :name%")
    Page<Series> findByNameLike(String name, Pageable pageable);

    @Query(value = "SELECT * FROM series s WHERE f_unaccent(s.name) ~>~ 'я%' and f_unaccent(s.name) !~~ 'я%' or f_unaccent(s.name) ~<~ 'a%' order by f_unaccent(s.name)", nativeQuery = true)
    Page<Series> findByFullNameLikeOther(Pageable pageable);
}
