package ru.hldns.klaw.web.model;

public interface AuthorCountDTO {
    String getFullName();

    String getDisplayName();

    long getCount();
}
