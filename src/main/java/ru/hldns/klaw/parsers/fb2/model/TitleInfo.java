package ru.hldns.klaw.parsers.fb2.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@ToString(exclude = "annotation")
@JsonIgnoreProperties({"annotation", "coverPage"})
public class TitleInfo {

    /**
     * Genre of this book
     */
    protected List<String> genre;

    /**
     * Author(s) of this book"
     */
    protected List<Person> author;

    /**
     * Book title"
     */
    protected String bookTitle;

    /**
     * Annotation for this book"
     */
    protected String annotation;

    /**
     * Any keywords for this book, intended for use in search engines"
     */
    protected List<String> keywords;

    /**
     * Date this book was written, can be not exact, e.g. 1863-1867. If an optional attribute is present, then it should contain some computer-readable date from the interval for use by search and indexingengines"
     */
    protected String date;

    /**
     * Any coverpage items, currently only images"
     */
    protected List<ImageType> coverPage;

    /**
     * Book's language"
     */
    protected String lang;

    /**
     * Book's source language if this is a translation"
     */
    protected String srcLang;

    /**
     * Translators if this is a translation"
     */
    protected List<Person> translator;
    /**
     * Any sequences this book might be part of"
     */
    protected List<SeriesBook> sequence;

    boolean hasCoverPage(String href) {
        if (coverPage != null)
            for (ImageType a : coverPage) {
                if (Objects.equals(a.href, href)) {
                    return true;
                }
            }
        return false;
    }

    boolean hasCoverPage() {
        return coverPage != null && coverPage.size() != 0;
    }

    void addBinary(Binary binary) {
        if (coverPage != null)
            for (ImageType a : coverPage) {
                if (Objects.equals(a.href, binary.id)) {
                    a.setBinary(binary);
                }
            }
    }

    public void addGenre(String elementText) {
        if (genre == null)
            genre = new ArrayList<>();
        genre.add(elementText);
    }

    public void addAuthor(Person parsePerson) {
        if (author == null)
            author = new ArrayList<>();
        author.add(parsePerson);
    }

    public void addSequence(SeriesBook parseSequence) {
        if (sequence == null)
            sequence = new ArrayList<>();
        sequence.add(parseSequence);
    }

    public void addTranslator(Person parsePerson) {
        if (translator == null)
            translator = new ArrayList<>();
        translator.add(parsePerson);
    }

    public void addCoverPage(ImageType parseCoverPage) {
        if (coverPage == null)
            coverPage = new ArrayList<>();
        coverPage.add(parseCoverPage);
    }
}
