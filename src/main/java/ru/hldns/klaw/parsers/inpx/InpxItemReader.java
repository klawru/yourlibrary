package ru.hldns.klaw.parsers.inpx;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamReader;
import ru.hldns.klaw.parsers.inpx.model.ParsedFile;
import ru.hldns.klaw.parsers.inpx.model.ParserServiceDAO;
import ru.hldns.klaw.utility.FileUtility;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Slf4j
public class InpxItemReader implements ItemStreamReader<String> {
    private static final String keyIndexFile = "inpFileIndex";
    private static final String keyLineNumber = "lineNumber";

    @Setter
    Charset charset = StandardCharsets.UTF_8;
    @Setter
    ParserServiceDAO service;

    private Path path;
    private ZipFile inpxFile;
    private List<ZipEntry> inpFiles;
    private ListIterator<LineIterator> iterator;
    private String inpFilename;
    private LineIterator current;
    private int lineNumber;

    private Map<String, ParsedFile> parsedFiles;

    @Override
    public String read() throws Exception {
        String line = null;
        if (current == null)
            return null;
        if (current.hasNext()) {
            line = current.next() + inpFilename;
            lineNumber++;
        }

        if (line == null) {
            saveLastParsedFile(true);
            current.close();
            if (iterator.hasNext()) {
                current = iterator.next();
                lineNumber = 0;
                inpFilename = FileUtility.getWithoutExtension(inpFiles.get(iterator.previousIndex()).getName());
                saveLastParsedFile(false);
                line = read();
                log.info("Read file {}. There are {} files left to parse.", inpFilename, (inpFiles.size() - iterator.previousIndex()));
            }
        }
        return line;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        //Проверяем валидность пути, и существование файла
        File file = path.toFile();
        if (!(file.isFile() && file.canRead()))
            throw new ItemStreamException("File path is invalid");

        try {
            //Открываем файл, как архив
            inpxFile = new ZipFile(file);
            inpFiles = toList(inpxFile.entries());
            if (inpFiles.size() == 0)
                throw new ItemStreamException("No one inp file in archive.");
            log.info("In inpx file '{}' content {} .inp files", file.getPath(), inpFiles.size());

            //Убираем завершенные спарсенные файлы
            removeParsedFiles();

            //Открываем для незавершенных файлов line Iterator
            List<LineIterator> inpStreams = new ArrayList<>();
            for (ZipEntry entry : inpFiles) {
                LineIterator lineIterator = IOUtils.lineIterator(new InputStreamReader(inpxFile.getInputStream(entry), charset));
                if (parsedFiles.containsKey(entry.getName())) {
                    //Если файл есть в списке, значит его не допарсили
                    final var parsedFile = parsedFiles.get(entry.getName());
                    for (int i = 0; i < parsedFile.getRows() && lineIterator.hasNext(); i++) {
                        lineIterator.next();
                    }
                    if (!lineIterator.hasNext()) {
                        parsedFile.setComplete(true);
                        parsedFiles.put(parsedFile.getName(), parsedFile);
                    }
                }
                if (lineIterator.hasNext())
                    inpStreams.add(lineIterator);
            }

            iterator = inpStreams.listIterator();
            if (iterator.hasNext()) {
                current = iterator.next();
                inpFilename = FileUtility.getWithoutExtension(inpFiles.get(0).getName());
            }
        } catch (IOException e) {
            throw new ItemStreamException("I/O error has occurred %s", e);
        }
    }

    private void removeParsedFiles() {
        //Загружаем из базы список спарсеных файлов
        parsedFiles = service.findAllParsedFile().stream().collect(Collectors.toMap(ParsedFile::getName, Function.identity()));

        final Iterator<ZipEntry> iterator = inpFiles.listIterator();
        while (iterator.hasNext()) {
            final var zipEntry = iterator.next();
            for (final ParsedFile parsedFile : parsedFiles.values()) {
                if (parsedFile.isComplete() && (zipEntry.getName().equals(parsedFile.getName()) || zipEntry.getCrc() == parsedFile.getHash())) {
                    iterator.remove();
                    if (log.isDebugEnabled())
                        log.trace("Remove inp files from parsing list parsing:{}", inpFiles.toString());
                }
            }
        }
        log.info("Inp files for parsing:{}", inpFiles.toString());
    }

    private List<ZipEntry> toList(Enumeration<? extends ZipEntry> entries) {
        ArrayList<ZipEntry> list = new ArrayList<>();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = entries.nextElement();
            if ("inp".equals(FileUtility.getExtension(zipEntry.getName())))
                list.add(zipEntry);
        }
        return list;
    }


    private void saveLastParsedFile(boolean isComplete) {
        if (iterator.previousIndex() >= 0) {
            ZipEntry entry = inpFiles.get(iterator.previousIndex());
            parsedFiles.put(entry.getName(), new ParsedFile(entry.getName(), entry.getCrc(), lineNumber, isComplete));
        }
    }

    private void saveContext() {
        service.saveParsedFiles(parsedFiles.values());
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
        executionContext.putInt(keyIndexFile, iterator.previousIndex());
        executionContext.putInt(keyLineNumber, lineNumber);
        if (iterator.previousIndex() >= 0) {
            ZipEntry entry = inpFiles.get(iterator.previousIndex());
            final var lastParsedFile = parsedFiles.get(entry.getName());
            if (lastParsedFile != null) {
                lastParsedFile.setRows(lineNumber);
                parsedFiles.put(entry.getName(), lastParsedFile);
            }
        }
        saveContext();
    }

    @Override
    public void close() throws ItemStreamException {
        try {
            log.info("Read end");
            if (inpxFile != null)
                inpxFile.close();
        } catch (IOException e) {
            throw new ItemStreamException("On close inpx file", e);
        }
    }

    public void setPath(String path) {
        this.path = Paths.get(path);
    }
}
