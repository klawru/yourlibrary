package ru.hldns.klaw.web.errors;

public class NotFoundUserException extends Exception {
    public NotFoundUserException(String s) {
        super((s));
    }
}
