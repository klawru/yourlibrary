package ru.hldns.klaw.web.service;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.repository.BookRepository;
import ru.hldns.klaw.utility.CacheEvictEvent;

@Service
public class AdminPageService {
    private final JobLauncher jobLauncher;
    private final Job parseInpxFileJob;
    private final Job parseFB2FilesJob;
    private final ApplicationEventPublisher publisher;
    private final BookRepository bookRepository;

    @Autowired
    public AdminPageService(@Qualifier("asyncJobLauncher") JobLauncher jobLauncher,
                            @Qualifier("parseInpxFileJob") Job parseInpxFileJob,
                            @Qualifier("parseFB2FilesJob") Job parseFB2FilesJob,
                            ApplicationEventPublisher publisher, BookRepository bookRepository) {

        this.jobLauncher = jobLauncher;
        this.parseInpxFileJob = parseInpxFileJob;
        this.parseFB2FilesJob = parseFB2FilesJob;
        this.publisher = publisher;
        this.bookRepository = bookRepository;
    }


    public void runParseInpxJob(JobParameters jobParameters) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        jobLauncher.run(parseInpxFileJob, jobParameters);
    }

    public void runParseFB2Job(JobParameters jobParameters) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        jobLauncher.run(parseFB2FilesJob, jobParameters);
    }

    public void allCacheEvict(){
        publisher.publishEvent(new CacheEvictEvent(this));
    }

    @Transactional
    public void clearDeletedBook() {
        bookRepository.deleteAllByDeletedTrue();
    }
}
