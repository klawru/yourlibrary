package ru.hldns.klaw.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.hldns.klaw.data.model.Genre;
import ru.hldns.klaw.web.service.CommunityService;
import ru.hldns.klaw.web.service.GenreService;

@Controller
public class CommunityController {

    private final CommunityService serviceDAO;
    private final GenreService genreService;

    @Autowired
    public CommunityController(CommunityService serviceDAO, GenreService genreService) {
        this.serviceDAO = serviceDAO;
        this.genreService = genreService;
    }

    @GetMapping("/community/{id}")
    public String community(@PathVariable("id") int communityId, Model model) {
        CommunityService.CommunityAndTopAuthors byId = serviceDAO.findCommunityAndTopAuthors(communityId);
        if (byId.isEmpty())
            return "redirect:/error/404";

        model.addAttribute("community", byId.getCommunity());
        model.addAttribute("authors", byId.getFacets());
        model.addAttribute("genres", byId.getGenres());
        return "community";
    }
}
