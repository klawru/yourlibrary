package ru.hldns.klaw.parsers.fb2.model;

import lombok.Data;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.ArrayList;
import java.util.List;

@Data
@Nullable
public class PublishInfo {
    /**
     * Original (paper) book name
     */
    protected String bookName;

    /**
     * Original (paper) book publisher
     */
    protected String publisher;

    /**
     * City where the original (paper) book was published
     */
    protected String city;

    /**
     * Year of the original (paper) publication
     */
    protected String year;

    protected String isbn;

    protected List<SeriesBook> sequence;

    public void addSequence(SeriesBook parseSequence) {
        if (sequence == null) {
            sequence = new ArrayList<>();
        }
        sequence.add(parseSequence);
    }
}
