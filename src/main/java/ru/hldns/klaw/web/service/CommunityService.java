package ru.hldns.klaw.web.service;

import lombok.Getter;
import org.hibernate.search.query.facet.Facet;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.Community;
import ru.hldns.klaw.data.repository.CommunityRepository;
import ru.hldns.klaw.data.repository.GenreRepository;
import ru.hldns.klaw.service.search.HibernateSearchService;
import ru.hldns.klaw.web.model.GenreDTOMin;

import java.util.List;
import java.util.Optional;

@Service
public class CommunityService {

    private final CommunityRepository communityRepository;
    private final GenreRepository genreRepository;
    private final HibernateSearchService searchService;

    public CommunityService(CommunityRepository communityRepository,
                            GenreRepository genreRepository,
                            HibernateSearchService searchService) {
        this.communityRepository = communityRepository;
        this.genreRepository = genreRepository;
        this.searchService = searchService;
    }


    @Cacheable(cacheNames = "listCommunity")
    public List<Community> findAllCommunity() {
        return communityRepository.findAll();
    }

    public Optional<Community> findForEditCommunityById(int id) {
        return communityRepository.findById(id);
    }

    @Cacheable("CommunityAndTopAuthors")
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CommunityAndTopAuthors findCommunityAndTopAuthors(int communityId) {
        final Optional<Community> community = communityRepository.findById(communityId);
        if (community.isEmpty()) {
            return CommunityAndTopAuthors.EMPTY;
        }
        final List<GenreDTOMin> genreDTOS = genreRepository.findByCommunityId(communityId, GenreDTOMin.class);
        final List<Facet> facets = searchService.topAuthorsByCommunity(communityId);
        return new CommunityAndTopAuthors(community.get(), genreDTOS, facets);
    }


    @Getter
    public static class CommunityAndTopAuthors {
        Community community;
        List<GenreDTOMin> genres;
        List<Facet> facets;
        final boolean isEmpty;

        CommunityAndTopAuthors(Community community, List<GenreDTOMin> genres, List<Facet> facets) {
            this.community = community;
            this.genres = genres;
            this.facets = facets;
            isEmpty = false;
        }

        private CommunityAndTopAuthors() {
            this.isEmpty = true;
        }

        final static CommunityAndTopAuthors EMPTY = new CommunityAndTopAuthors();
    }

}
