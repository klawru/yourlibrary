package ru.hldns.klaw.web.controller;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.hldns.klaw.utility.FieldMatch;
import ru.hldns.klaw.web.errors.NotFoundUserException;
import ru.hldns.klaw.web.service.UserSettingsPageService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.security.Principal;

@Controller
public class UserSettingsController {

    private final UserSettingsPageService userService;

    @Autowired
    public UserSettingsController(UserSettingsPageService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String getSettings(Model model, Principal principal) {
        model.addAttribute("passwordForm", new PasswordForm());
        model.addAttribute("oauth", false);
        return "userSettings";
    }


    @RequestMapping(value = "/settings", method = RequestMethod.POST)
    public String changeUserPassword(@Valid @ModelAttribute("passwordForm") PasswordForm passwordForm, BindingResult result,
                                     Principal principal, Model model) throws NotFoundUserException {
        if (result.hasErrors())
            return "userSettings";
        userService.changeUserPassword(principal.getName(), passwordForm.currentPassword, passwordForm.confirmation, result, model);
        return "userSettings";
    }


    @Data
    @FieldMatch(first = "newPassword", second = "confirmation", errorMessage = "error.passwordConfirm")
    public static class PasswordForm {
        @Size(min = 4, max = 100)
        @NotNull(message = "err.whitespace")
        private String currentPassword;
        @NotNull(message = "err.whitespace")
        @Size(min = 4, max = 100, message = "err.PasswordSize")
        private String newPassword;
        @NotNull(message = "err.whitespace")
        private String confirmation;
    }

}
