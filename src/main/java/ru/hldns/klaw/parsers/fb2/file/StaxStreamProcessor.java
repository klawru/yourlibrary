package ru.hldns.klaw.parsers.fb2.file;

import com.fasterxml.aalto.AsyncXMLInputFactory;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

public class StaxStreamProcessor implements AutoCloseable {

    private final XMLStreamReader reader;
    private final InputStream inputFile;

    public StaxStreamProcessor(InputStream is, AsyncXMLInputFactory factory) throws XMLStreamException {
        inputFile = is;
        reader = factory.createXMLStreamReader(is);
    }

    public XMLStreamReader getReader() {
        return reader;
    }

    @Override
    public void close() throws Exception {
        if (reader != null) {
            try {
                reader.close();
            } catch (XMLStreamException ignored) {
            }
        }
        inputFile.close();
    }


}
