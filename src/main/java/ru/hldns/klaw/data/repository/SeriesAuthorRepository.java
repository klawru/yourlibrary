package ru.hldns.klaw.data.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.hldns.klaw.data.model.SeriesAuthor;
import ru.hldns.klaw.web.model.SeriesDTO;

import java.util.List;

public interface SeriesAuthorRepository extends JpaRepository<SeriesAuthor, Integer> {
    List<SeriesDTO> getAllByIdAuthor(int id, Sort sort);

    @Query(value = "REFRESH MATERIALIZED VIEW series_author", nativeQuery = true)
    void refreshView();
}